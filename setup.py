#!/usr/bin/env python
from distutils.core import setup
import os
from collections import defaultdict

__version__ = "0.0.7"

def get_files(prefix):
    r = []
    for root, dirnames, files in os.walk(prefix):
        x = [os.path.join(root, f) for f in files]
        if x:
            r += [(root, x)]
    return r

setup(
    name="noc-pkg-termjs",
    version=__version__,
    description="termjs package for NOC",
    url="https://github.com/chjj/term.js/",
    maintainer="nocproject.org",
    maintainer_email="pkg@nocproject.org",
    data_files=get_files("static/pkg/termjs")
)
